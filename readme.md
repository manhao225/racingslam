# Racing SLAM
SCUTRacing 新一代 SLAM 程序

# 依赖
+ `roscpp`
+ `fsd_common_msgs`
+ `std_msgs`
+ `geometry_msgs`
+ `racingtftree`
+ `pcl` & `pcl_ros`
+ `Eigen`

# 构建
1. 将仓库部署到一个包含依赖包的工作空间下
2. 如果安装了`catkin_tools`: `catkin build`，没安装就：`catkin_make`