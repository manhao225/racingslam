#pragma once

#include "cone_in_slam.h"
#include "fsd_common_msgs/Map.h"
#include "fsd_common_msgs/CarState.h"
#include "fsd_common_msgs/ConeDetections.h"
#include "fsd_common_msgs/ResAndAmi.h"
#include "racingslam/CarTrack.h"

#include <ros/ros.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <nav_msgs/Path.h>

#include <vector>
#include <string>
#include <fstream>
#include <iostream>

namespace racingslam
{
    class Mapping
    {
    public:
        Mapping(std::string ns);
        void regTopic();
        void loadParam();
        fsd_common_msgs::ConeDetections getMap();
        fsd_common_msgs::Map getMapWithColor();

        void startSlam();

    private:

        //sub & pub
        ros::NodeHandle node_;
        ros::Subscriber cone_sub_;
        ros::Subscriber state_sub_;
        ros::Subscriber track_sub_;
        ros::Subscriber res_sub_;
        ros::Publisher map_pub_;
        ros::Publisher gps_line_pub_;
        ros::Publisher map_visualization_pub_;
        ros::Publisher track_visualization_pub_;
        ros::Publisher current_visual_cone_detections_pub_;
        ros::Publisher realtime_map_pub_;


        //以下参数会随着程序的运行发生改变，为中间量
        std::vector<ConeInSlam> map_buffer_;
        std::vector<fsd_common_msgs::ConeDetections> cone_buffer_;
        fsd_common_msgs::CarState current_state_;
        fsd_common_msgs::CarState last_state_;
        fsd_common_msgs::CarState init_state_;
        racingslam::CarTrack track_;
        nav_msgs::Path gps_line_;
        double odom_;
        double delta_dis_;
        double loop_detection_dis_;
        int loop_detection_timer_;
        bool is_state_init_; //用于回环检测
        bool is_track_line_init_; //用于记录轨迹
        bool is_loop_closed_;
        bool is_map_saved_;
        bool is_map_load_;
        int cone_run_count_; //用于记录椎桶回调函数运行次数，每五次运行一次椎桶过滤器
        nav_msgs::Path track_line_;
        int res_state_; //用于检测res是否给出go信号
        
        //以下参数是人为设置的参数，不会随着程序的运行而改变
        double reg_range_;
        int min_update_count_;
        double max_reg_dis_;
        double min_cone_distance_;
        double lidar2cog_length_; //雷达到质心的长度 A10为2.4
        int loop_detection_timer_count_;
        std::string cone_sub_topic_;
        std::string state_sub_topic_;
        std::string track_sub_topic_;
        std::string res_sub_topic_;
        std::string map_pub_topic_;
        std::string gps_line_pub_topic_;
        std::string map_visualization_pub_topic_;
        std::string track_visualization_pub_topic_;
        std::string realtime_map_pub_topic_;
        std::string current_visual_cone_detections_pub_topic_;
        std::string frame_id_;
        std::string map_save_path_;
        bool odom_track_switch_;//是否使用里程计轨迹
        bool map_load_switch_;  //是否使用地图存储


        fsd_common_msgs::ConeDetections translateConeState(const fsd_common_msgs::ConeDetections& original_cones, fsd_common_msgs::CarState& car_state);
        void coneFilter();
        void coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg);
        void stateCallback(const fsd_common_msgs::CarState &msg);
        void trackCallback(const racingslam::CarTrack::ConstPtr& msg);
        void resCallback(const fsd_common_msgs::ResAndAmi& msg);
        void regCones();
        void loopDetect();
        double getDeltaDis();
        void sendVisualization(std::string frame_id);
        //void sendCarTrackVisualization(std::string frame_id, fsd_common_msgs::CarState car_state);
        void sendVisualCurrentConeDetections(std::string frame_id, fsd_common_msgs::ConeDetections cones);
        void saveMap();
        void loadMap();
        nav_msgs::Path carstateTrack2Path();

        //私有方法
        double dis(const fsd_common_msgs::Cone a){return sqrt(a.position.x*a.position.x + a.position.y*a.position.y);}
        double dis(const fsd_common_msgs::Cone a, const fsd_common_msgs::Cone b)
        {
            return sqrt((a.position.x - b.position.x)*(a.position.x - b.position.x) + (a.position.y - b.position.y)*(a.position.y - b.position.y));
        }
        double dis(const ConeInSlam& a, const ConeInSlam& b)
        {
            geometry_msgs::Point a_pos = a.getPosition();
            geometry_msgs::Point b_pos = b.getPosition();

            return sqrt((a_pos.x-b_pos.x)*(a_pos.x-b_pos.x) + (a_pos.y-b_pos.y)*(a_pos.y-b_pos.y));
        }
        double dis(const fsd_common_msgs::CarState a)
        {
            return sqrt((a.car_state.x - init_state_.car_state.x)*(a.car_state.x - init_state_.car_state.x) + (a.car_state.y - init_state_.car_state.y)*(a.car_state.y - init_state_.car_state.y));
        }
    };
}