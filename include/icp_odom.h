#pragma once

#include"odometry.h"
#include"fsd_common_msgs/ConeDetections.h"

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>
#include <pcl/common/transforms.h>

#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <sstream>

namespace racingslam
{
    class ICPOdometry : public Odometry
    {
        public:
            ICPOdometry(std::string ns);

            void loadParam();

            void regTopics();        //注册话题
            
            void coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg);

            void calculateCarState();

            void sendVisualization(std::string frame_id); //发送可视化

        private:

            //从参数服务器加载的参数
            double min_dis2update_;
            double max_dis2pass_;
            double max_iterations_;
            double convergence_condition_;
            double min_pointcloud_num_;
            std::string cone_topic_;
            
            //算法中用到的参数
            bool is_last_cones_init_;
            bool is_transform_mat_init_;
            double current_min_dis_;
            double last_min_dis_;
            fsd_common_msgs::ConeDetections current_cones_; //当前接受到的最后的椎桶信息
            fsd_common_msgs::Cone current_min_dis_cone_;
            fsd_common_msgs::ConeDetections last_cones_;//上一次接受到的椎桶消息
            fsd_common_msgs::Cone last_min_dis_cone_;

            pcl::PointCloud<pcl::PointXYZ>::Ptr current_cones_pcl_;
            pcl::PointCloud<pcl::PointXYZ>::Ptr last_cones_pcl_;

            Eigen::Matrix4f transformation_;
            Eigen::Matrix4f original_transformation_;

            double odom_delta_x_;
            double odom_delta_y_;
            double odom_delta_theta_;

            ros::Publisher cones_visualization_pub_;
            ros::Publisher odom_track_visualization_pub_;
            ros::Subscriber cone_sub_;

            //private methon
            double dis(const fsd_common_msgs::Cone a){return sqrt(a.position.x*a.position.x + a.position.y*a.position.y);}
            double dis(const fsd_common_msgs::Cone a, const fsd_common_msgs::Cone b)
            {
                return sqrt((a.position.x - b.position.x)*(a.position.x - b.position.x) + (a.position.y - b.position.y)*(a.position.y - b.position.y));
            }
    };
}