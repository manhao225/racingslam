#pragma once

#include "odometry.h"
#include "racingslam/CarTrack.h"

#include <sensor_msgs/PointCloud2.h>
#include <pcl/registration/ndt.h>

namespace racingslam {

    class NDTOdometry : public Odometry {
        public:

            NDTOdometry(std::string ns);
            void loadParam();
            void regTopics();

            void inputCallback(const sensor_msgs::PointCloud2::ConstPtr &msg);

        private:

            // pub & sub
            ros::Subscriber input_sub_;
            ros::Publisher track_pub_;

            std::string input_topic_name_;

            // pcl
            pcl::PointCloud<pcl::PointXYZ>::Ptr last_cloud_;
            pcl::PointCloud<pcl::PointXYZ>::Ptr current_cloud_;
            pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>::Ptr ndt_;

            // ndt param
            double min_diff_;       // min difference for termination condition. 
            double step_;           // max step for More-Thuente line search. 
            double resolution_;     // grid size of ndt. (m)
            double max_iter_;       // max iteration of registration

            // private methods
            void initNDT();

    };

}