#include "mapping.h"

using namespace racingslam;

Mapping::Mapping(std::string ns)
{
    node_ = ros::NodeHandle(ns);
    delta_dis_ = 0;
    odom_ = 0;
    loop_detection_timer_ = -1;
    cone_run_count_ = 0;
    res_state_ = 0;
    is_state_init_ = false;
    is_track_line_init_ = false;
    is_loop_closed_ = false;
    is_map_saved_ = false;
    is_map_load_ = false;
}

/***
 * @brief 该函数用于加载参数服务器的参数
*/
void Mapping::loadParam()
{
    if(!node_.param("mapping/odom_track_switch", odom_track_switch_, false))
    {
        ROS_WARN_STREAM("Did not load mapping/odom_track_switch. Standard value is: " << odom_track_switch_);
    }
    if(!node_.param("mapping/map_load_switch", map_load_switch_, false))
    {
        ROS_WARN_STREAM("Did not load mapping/map_load_switch. Standard value is: " << map_load_switch_);
    }
    if(!node_.param<std::string>("mapping/res_sub_topic", res_sub_topic_, "/res_and_ami"))
    {
        ROS_WARN_STREAM("Did not load mapping/res_sub_topic. Standard value is: " << res_sub_topic_);
    }
    if (!node_.param<std::string>("mapping/cone_sub_topic", cone_sub_topic_, "/perception/lidar/cone_side")) 
    {
        ROS_WARN_STREAM("Did not load mapping/cone_sub_topic. Standard value is: " << cone_sub_topic_);
    }
    if(!node_.param<std::string>("mapping/state_sub_topic", state_sub_topic_, "/estimation/slam/state"))
    {
        ROS_WARN_STREAM("Did not load mapping/state_sub_topic. Standard value is: " << state_sub_topic_);
    }
    if(!node_.param<std::string>("odometry/odom_track_topic", track_sub_topic_, "/racingslam/odom_track"))
    {
        ROS_WARN_STREAM("Did not load odometry/odom_track_topic. Standard value is: " << track_sub_topic_);
    }
    if(!node_.param<std::string>("mapping/map_pub_topic", map_pub_topic_, "/estimation/slam/map"))
    {
        ROS_WARN_STREAM("Did not load mapping/map_pub_topic. Standard value is: " << map_pub_topic_);
    }
    if(!node_.param<std::string>("mapping/gps_line_pub_topic", gps_line_pub_topic_, "/estimation/slam/gps_line"))
    {
        ROS_WARN_STREAM("Did not load mapping/gps_line_pub_topic. Standard value is: " << gps_line_pub_topic_);
    }
    if(!node_.param<std::string>("mapping/map_visualization_pub_topic", map_visualization_pub_topic_, "/visualization/slam/map"))
    {
        ROS_WARN_STREAM("Did not load mapping/map_visualization_pub_topic. Standard value is: " << map_visualization_pub_topic_);
    }
    if(!node_.param<std::string>("mapping/track_visualization_pub_topic", track_visualization_pub_topic_, "/visualization/slam/track"))
    {
        ROS_WARN_STREAM("Did not load mapping/track_visualization_pub_topic. Stand value is: " << track_visualization_pub_topic_);
    }
    if(!node_.param<std::string>("mapping/current_visual_cone_detections_pub_topic", current_visual_cone_detections_pub_topic_, "/visualization/slam/cone_detections"))
    {
        ROS_WARN_STREAM("Did not load mapping/current_visual_cone_detections_pub_topic. Stand value is: " << current_visual_cone_detections_pub_topic_);
    }
    if(!node_.param<std::string>("mapping/realtime_map_pub_topic", realtime_map_pub_topic_, "/estimation/slam/realtime_map"))
    {
        ROS_WARN_STREAM("Did not load mapping/realtime_map_pub_topic. Standard value is: " << realtime_map_pub_topic_);
    }
    if(!node_.param<std::string>("mapping/map_save_path", map_save_path_, "/home/manhao/racing_ws/map/"))
    {
        ROS_WARN_STREAM("Did not load mapping/map_save_path. Standard value is: " << map_save_path_);
    }
    if(!node_.param("mapping/reg_range", reg_range_, 102.25))
    {
        ROS_WARN_STREAM("Did not load mapping/reg_range. Standard value is: " << reg_range_);
    }
    if(!node_.param("mapping/min_update_count", min_update_count_, 3))
    {
        ROS_WARN_STREAM("Did not load mapping/min_update_count. Standard value is: " << min_update_count_);
    }
    if(!node_.param("mapping/max_reg_dis", max_reg_dis_, 1.5))
    {
        ROS_WARN_STREAM("Did not load mapping/max_reg_dis. Standard valus is: " << max_reg_dis_);
    }
    if(!node_.param("mapping/min_cone_distance", min_cone_distance_, 1.5))
    {
        ROS_WARN_STREAM("Did noe load mapping/min_cone_distance. Standard value is: " << min_cone_distance_);
    }
    if(!node_.param("mapping/loop_detection_timer_count", loop_detection_timer_count_, 10))
    {
        ROS_WARN_STREAM("Did not load mapping/loop_detection_time_count. Standard value is: " << loop_detection_timer_count_);
    }
    if(!node_.param("mapping/loop_detection_dis", loop_detection_dis_, 2.0))
    {
        ROS_WARN_STREAM("Did not load mapping/loop_detection_dis. Stand value is: " << loop_detection_dis_);
    }
    if(!node_.param<std::string>("mapping/visualization_frame_id", frame_id_, "world"))
    {
        ROS_WARN_STREAM("Did not load mapping/visualization_frame_id. Standard value is: " << frame_id_);
    }
    if(!node_.param("mapping/lidar2cog_length", lidar2cog_length_, 2.4))
    {
        ROS_WARN_STREAM("Did not load mapping/lidar2cog_length. Stand value is: " << lidar2cog_length_);
    }
}

/***
 * @brief 该函数用于接收与注册话题
*/
void Mapping::regTopic()
{
    cone_sub_ = node_.subscribe<fsd_common_msgs::ConeDetections>(cone_sub_topic_, 1, &Mapping::coneCallback, this);
    state_sub_ = node_.subscribe(state_sub_topic_, 1, &Mapping::stateCallback, this);
    res_sub_ = node_.subscribe(res_sub_topic_, 1, &Mapping::resCallback, this);
    if(odom_track_switch_) track_sub_ = node_.subscribe<racingslam::CarTrack>(track_sub_topic_, 1, &Mapping::trackCallback, this); //开关控制是否使用里程计
    
    map_pub_ = node_.advertise<fsd_common_msgs::Map>(map_pub_topic_, 10, true);
    realtime_map_pub_ = node_.advertise<fsd_common_msgs::ConeDetections>(realtime_map_pub_topic_, 10, true); //实时全局地图
    gps_line_pub_ = node_.advertise<nav_msgs::Path>(gps_line_pub_topic_, 10, true);

    map_visualization_pub_ = node_.advertise<visualization_msgs::MarkerArray>(map_visualization_pub_topic_, 10, true);//全局地图可视化
    //track_visualization_pub_ = node_.advertise<nav_msgs::Path>(track_visualization_pub_topic_, 10, true);//轨迹可视化
    current_visual_cone_detections_pub_ = node_.advertise<visualization_msgs::MarkerArray>(current_visual_cone_detections_pub_topic_, 10, true);//当前椎桶可视化
}

/***
 * @brief 将椎桶从雷达坐标系转换到世界坐标系
*/
fsd_common_msgs::ConeDetections Mapping::translateConeState(const fsd_common_msgs::ConeDetections& original_cones, fsd_common_msgs::CarState& car_state)
{
    fsd_common_msgs::ConeDetections target_cones;

    for(auto i : original_cones.cone_detections)
    {
        if(i.position.x*i.position.x + i.position.y*i.position.y > reg_range_) continue; //限制椎桶数量为前3对椎桶
        fsd_common_msgs::Cone temp;
        temp.color = i.color;
        temp.colorConfidence = i.colorConfidence;
        temp.poseConfidence = i.poseConfidence;
        temp.position.x = car_state.car_state.x + (i.position.x + lidar2cog_length_)*cos(car_state.car_state.theta) - (i.position.y)*sin(car_state.car_state.theta);
        temp.position.y = car_state.car_state.y + (i.position.y)*cos(car_state.car_state.theta) + (i.position.x + lidar2cog_length_)*sin(car_state.car_state.theta);

        target_cones.cone_detections.push_back(temp);
    }

    return target_cones;
}

/***
 * @brief 该函数根据轨迹对椎桶进行坐标转换，得到轨迹时进行一次建图
*/
void Mapping::regCones()
{
    //ROS_INFO("cone buffer size = %d", cone_buffer_.size());

    if(!is_state_init_)
    {
        return;
    }

    for(auto i = cone_buffer_.begin(); i != cone_buffer_.end(); i++)
    {
        double time_cone = i->header.stamp.toSec();

        double min_delta_time_1 = std::numeric_limits<double>::max();
        std::vector<geometry_msgs::PoseStamped>::iterator flag_1; //记录选中的轨迹中的点
        // std::shared_ptr<fsd_common_msgs::CarState> flag_2;
        // std::shared_ptr<fsd_common_msgs::CarState> flag_3;

        for(auto j = track_line_.poses.begin(); j != track_line_.poses.end(); j++)
        {
            double time_track = j->header.stamp.toSec();
            double delta_time = std::abs(time_cone - time_track);
            if(delta_time < min_delta_time_1)
            {
                min_delta_time_1 = delta_time;
                flag_1 = j;
                // flag_2 = std::make_shared<fsd_common_msgs::CarState>(*(j - 1));
                // flag_3 = std::make_shared<fsd_common_msgs::CarState>(*(j + 1));
            }
        }

        fsd_common_msgs::CarState this_state;
        // this_state.car_state.x = (flag_1->car_state.x + flag_2->car_state.x + flag_3->car_state.x) / 3;
        // this_state.car_state.y = (flag_2->car_state.y + flag_2->car_state.y + flag_3->car_state.x) / 3;
        // this_state.car_state.theta = (flag_1->car_state.theta + flag_2->car_state.theta + flag_3->car_state.theta) / 3;

        this_state.car_state.x = flag_1->pose.position.x;
        this_state.car_state.y = flag_1->pose.position.y;
        this_state.car_state.theta = flag_1->pose.orientation.z;

        // ROS_INFO("cone detections time : %f \n state time : %f", (i->header.stamp.toSec())/1000, (flag_1->header.stamp.toSec())/1000);

        //ROS_INFO("this state x = %f, y = %f ", this_state.car_state.x, this_state.car_state.y);

        //ROS_INFO("befor translate cone detection x = %f, y = %f", i->cone_detections[0].position.x, i->cone_detections[0].position.y);

        fsd_common_msgs::ConeDetections temp_cone_detections = translateConeState(*i, this_state);

        //ROS_INFO("after translate cone detection x = %f, y = %f", temp_cone_detections.cone_detections[0].position.x, temp_cone_detections.cone_detections[0].position.y);
        
        //将椎桶送入map_buffer
        if(map_buffer_.empty())
        {
            for(auto j: temp_cone_detections.cone_detections)
            {
                if(j.poseConfidence.data > 0.8)
                {
                    ConeInSlam temp(j);
                    map_buffer_.push_back(temp);
                }
            }
        }
        else
        {
            for(auto j: temp_cone_detections.cone_detections)
            {
                if(j.poseConfidence.data > 0.8)
                {
                    double min_dis = std::numeric_limits<double>::max();
                    std::vector<ConeInSlam>::iterator cone_to_update;
                    for(auto k = map_buffer_.begin(); k != map_buffer_.end(); k++)
                    {
                        fsd_common_msgs::Cone temp = k->getCone();
                        double dis = Mapping::dis(j,temp);
                        if(dis < min_dis)
                        {
                            min_dis = dis;
                            cone_to_update = k;
                        }
                    }
                    if(min_dis <= max_reg_dis_)
                    {
                        //std::cout << "#################################################################" << std::endl;
                        //double x = cone_to_update->getPosition().x; double y = cone_to_update->getPosition().y;
                        //ROS_INFO("update cone x = %f, y = %f ", x, y);

                        cone_to_update -> update(j);

                        //ROS_INFO("update times: %d", cone_to_update->getUpdateCount());
                        //x = cone_to_update->getPosition().x; y = cone_to_update->getPosition().y;
                        //ROS_INFO("new position is x = %f, y = %f ", x, y);
                        //ROS_INFO("map buffer size = %d", map_buffer_.size());
                    }
                    else
                    {
                        ConeInSlam temp(j);
                        map_buffer_.push_back(temp);
                        //ROS_INFO("%f, %f, put in map buffer", temp.getPosition().x, temp.getPosition().y);
                        //ROS_INFO("map buffer size = %d", map_buffer_.size());
                    }
                }
            }
        }
    }
        
    
}

/**
 * @brief 对椎桶进行滤波，将两个距离过近的椎桶取平均值之后合并
 * 
 */
void Mapping::coneFilter() 
{

    ROS_INFO("Start cone filter !");

    auto map_buffer_end = map_buffer_.end();
    for(auto i = map_buffer_.begin(); i != map_buffer_end; i++) {

        double min_dis = std::numeric_limits<double>::max();
        std::vector<ConeInSlam>::iterator cone_to_del;

        for(auto j = map_buffer_.begin(); j != map_buffer_.end(); j++) {

            if(i == j) continue;

            double current_dis = dis(*i, *j);
            if(current_dis < min_dis && current_dis <= min_cone_distance_ && i->getCone().color == j->getCone().color) {
                min_dis = current_dis;
                cone_to_del = j;
            }

        }

        if(min_dis == std::numeric_limits<double>::max()) {
            continue;
        }

        geometry_msgs::Point i_pos = i->getPosition();
        geometry_msgs::Point other_pos = cone_to_del->getPosition();
        i->setPosition((i_pos.x+other_pos.x)/2, (i_pos.y+other_pos.y)/2);
        map_buffer_.erase(cone_to_del);
    }
}

/***
 * @brief 该函数用于进行回环检测，若检测到已经完成回环，则发送真值
*/
void Mapping::loopDetect()
{    
    double current_state_dis = dis(current_state_);
    double last_state_dis = dis(last_state_);
    delta_dis_ = (delta_dis_ + (current_state_dis - last_state_dis)) / 2;

    //ROS_INFO("current dis: %f; last dis: %f, delta: %f", current_state_dis, last_state_dis, delta_dis_);

    if(current_state_dis <= loop_detection_dis_ && odom_ >= 10*loop_detection_dis_) 
    {

        ROS_INFO("test");
        if(delta_dis_ > 0 && loop_detection_timer_ < 0)
        {
            ROS_INFO("TIMER ACTIVE! ");
            loop_detection_timer_ = 0;
        }
        else if(delta_dis_ > 0 && loop_detection_timer_ >= 0)
        {
            loop_detection_timer_++;
        }
        else if(delta_dis_ < 0 && loop_detection_timer_ >= 0)
        {
            ROS_INFO("TIMER_INACTIVE! ");
            loop_detection_timer_ = -1;
        }

        if(loop_detection_timer_ >= loop_detection_timer_count_)
        {
            is_loop_closed_ = true;
        }

    }
    else 
    {

        if(loop_detection_timer_ >= 0)
        {
            ROS_INFO("TIMER_INACTIVE! ");
            loop_detection_timer_ = -1;
        }
    }
}

void Mapping::resCallback(const fsd_common_msgs::ResAndAmi& msg)
{
    res_state_ = msg.resState;
}

/***
 * @brief 椎桶的回调函数
*/
void Mapping::coneCallback(const fsd_common_msgs::ConeDetections::ConstPtr& msg)
{
    //在没有给出go信号之前不运行
    // if(res_state_ == 0)
    // {
    //     return;
    // }
    
    //若使用地图加载，则不使用原椎桶回调函数
    if(map_load_switch_)
    {
        return;
    }
    
    ros::Time start_time = ros::Time::now();
    
    cone_run_count_++;//记录椎桶回调函数运行次数，用于对map_buffer_进行滤波

    fsd_common_msgs::ConeDetections temp;
    temp.cone_detections = msg->cone_detections;
    temp.header = msg->header;

    cone_buffer_.push_back(temp);
    //ROS_INFO("time %f cones was put in buffer", temp.header.stamp.toSec());
    //ROS_INFO("%d", cone_buffer_.size());
    if(!map_buffer_.empty() && cone_run_count_ % 1 == 0)
    {
        coneFilter();
    }

    sendVisualCurrentConeDetections(frame_id_, temp);

    if(!is_loop_closed_) startSlam();


    if(is_loop_closed_)
    {
        //fsd_common_msgs::Map map;
        //map = Mapping::getMapWithColor();
        map_pub_.publish(getMapWithColor());
        ROS_WARN("================== map have been published ==================");

        if(!is_map_saved_)
        {
            saveMap();
            is_map_saved_ = true;
        }
        ROS_WARN("==================== map have been saved ====================");
    }

    ros::Time end_time = ros::Time::now();
    double mapping_delay = end_time.toSec() - start_time.toSec();

    ROS_INFO("mapping delay = %f", mapping_delay*1000);
}

/***
 * @brief 该函数是车辆位置的回调函数，用于处理接受到的车辆的位姿信息
*/
void Mapping::stateCallback(const fsd_common_msgs::CarState &msg)
{
    //在没有给出go信号之前不运行
    // if(res_state_ == 0)
    // {
    //     return;
    // }
    
    //若使用地图加载，则不运行该回调函数
    if(map_load_switch_)
    {
        if(!is_map_load_)
        {
            loadMap();
            is_map_load_ = true;
        }
        fsd_common_msgs::Map map;
        map = Mapping::getMapWithColor();
        map_pub_.publish(map);
        ROS_INFO("================== map have been published ==================");
        Mapping::sendVisualization(frame_id_);
        track_line_.header.frame_id = frame_id_;
        gps_line_pub_.publish(track_line_);
        
        return;
    }

    if(!odom_track_switch_ && !is_loop_closed_)
    {
        geometry_msgs::PoseStamped temp_pose;
        double delta_dis = 0.0;
        if(!is_track_line_init_)
        {
            track_line_.header.frame_id = frame_id_;

            temp_pose.header = msg.header;
            temp_pose.pose.position.x = msg.car_state.x;
            temp_pose.pose.position.y = msg.car_state.y;
            temp_pose.pose.orientation.z = msg.car_state.theta;
            track_line_.poses.push_back(temp_pose);
            gps_line_pub_.publish(track_line_);
            
            is_track_line_init_ = true;

            return;
            //sendCarTrackVisualization(frame_id_, *msg);
        }

        delta_dis = sqrt(pow(msg.car_state.x - (track_line_.poses.end() - 1) -> pose.position.x, 2) + pow(msg.car_state.y - (track_line_.poses.end() - 1) -> pose.position.y, 2)); //计算当前点与上一点之间的位移，若过小则不计入track_line_
        
        if(is_track_line_init_ && delta_dis > 0.05)
        {
            temp_pose.header = msg.header;
            temp_pose.pose.position.x = msg.car_state.x;
            temp_pose.pose.position.y = msg.car_state.y;
            temp_pose.pose.orientation.z = msg.car_state.theta;
            track_line_.poses.push_back(temp_pose);
            gps_line_pub_.publish(track_line_);
            //sendCarTrackVisualization(frame_id_, *msg);
            //ROS_INFO("time: %f ,put in track", msg->header.stamp.toSec());
            //ROS_INFO("x = %f, y = %f ", msg->car_state.x, msg->car_state.y);
        }
    }

    ROS_INFO("last car track x: %f, y: %f", (track_line_.poses.end() - 1) -> pose.position.x, (track_line_.poses.end() - 1) -> pose.position.y);
    
    if(!is_state_init_)
    {
        init_state_.car_state.x     = msg.car_state.x + 1.0*cos(msg.car_state.theta);
        init_state_.car_state.y     = msg.car_state.y + 1.0*sin(msg.car_state.theta);
        init_state_.car_state.theta = 0 * msg.car_state.theta;
        init_state_.header = msg.header;

        current_state_ = init_state_;
        last_state_ = init_state_;

        is_state_init_ = true;
    }

    if(is_state_init_)
    {
        last_state_ = current_state_;

        current_state_.car_state.x     = msg.car_state.x + 1.0*cos(msg.car_state.theta); //将车辆质心位置转换到雷达坐标位置
        current_state_.car_state.y     = msg.car_state.y + 1.0*sin(msg.car_state.theta);
        current_state_.car_state.theta = 0 * msg.car_state.theta;
        current_state_.header = msg.header;

        // ROS_INFO("last car state x = %f, y = %f, theta = %f", last_state_.car_state.x, last_state_.car_state.y, last_state_.car_state.theta);
        // ROS_INFO("current car state x = %f, y = %f, theta = %f", current_state_.car_state.x, current_state_.car_state.y, current_state_.car_state.theta);

        odom_ += getDeltaDis();
        //ROS_INFO("odom = %f", odom_);

        Mapping::loopDetect();
    }

    ROS_INFO("-------------------------------------------------------------");
    if(!is_loop_closed_)
    {
        ROS_WARN("is loop close: 0");
    }
    if(is_loop_closed_)
    {
        ROS_INFO("is loop close: 1");
    }
}

/***
 * @brief 轨迹回调函数，用于接收轨迹
*/
void Mapping::trackCallback(const racingslam::CarTrack::ConstPtr& msg)
{
    //在没有给出go信号之前不运行
    // if(res_state_ == 0)
    // {
    //     return;
    // }
    
    //若使用地图加载，则不运行该回调函数
    if(map_load_switch_)
    {
        return;
    }
    track_ = *msg;
}

/***
 * @brief 该函数用来发送可视化椎桶地图话题信息
*/
void Mapping::sendVisualization(std::string frame_id)
{
    visualization_msgs::Marker cone_marker;
    visualization_msgs::MarkerArray cone_maker_array;

    //Set the marker type
    cone_marker.type = visualization_msgs::Marker::CUBE;
    cone_marker.action = visualization_msgs::Marker::ADD;

    cone_marker.header.stamp = ros::Time::now();
    cone_marker.header.frame_id = frame_id;
														   
    cone_marker.ns = "slam";

    cone_marker.lifetime = ros::Duration();
    cone_marker.frame_locked = true;

    //Set the color
    cone_marker.color.r = 1.0f;
    cone_marker.color.g = 0.0f;
    cone_marker.color.b = 0.0f;
    cone_marker.color.a = 0.8f;

    cone_marker.scale.x = 0.2;
    cone_marker.scale.y = 0.2;
    cone_marker.scale.z = 0.3;

    // default val to fix warn of rviz
    cone_marker.pose.orientation.x = 0;
    cone_marker.pose.orientation.y = 0;
    cone_marker.pose.orientation.z = 0;
    cone_marker.pose.orientation.w = 1;

    fsd_common_msgs::ConeDetections map;
    map = Mapping::getMap();

    for(auto i: map.cone_detections)
    {
        cone_marker.id++;
        cone_marker.pose.position.x = i.position.x;
        cone_marker.pose.position.y = i.position.y;

        if(i.color.data == "r")
        {
            cone_marker.color.r = 1.0f;
            cone_marker.color.g = 0.0f;
            cone_marker.color.b = 0.0f;
        }
        else if(i.color.data == "b")
        {
            cone_marker.color.r = 0.0f;
            cone_marker.color.g = 0.0f;
            cone_marker.color.b = 1.0f;
        }
        else
        {
            cone_marker.color.r = 0.0f;
            cone_marker.color.g = 1.0f;
            cone_marker.color.b = 0.0f;
        }

        cone_maker_array.markers.push_back(cone_marker);
    }

    map_visualization_pub_.publish(cone_maker_array);
}

// /***
//  * @brief 该函数对车辆轨迹进行可视化输出
// */
// void Mapping::sendCarTrackVisualization(std::string frame_id, fsd_common_msgs::CarState car_state)
// {
//     track_line_.header.frame_id = frame_id;
//     track_line_.header.stamp = ros::Time::now();

//     geometry_msgs::PoseStamped temp_point;
//     temp_point.pose.position.x = car_state.car_state.x;
//     temp_point.pose.position.y = car_state.car_state.y;
//     track_line_.poses.push_back(temp_point);

//     track_visualization_pub_.publish(track_line_);

//     return;
// }

/***
 * @brief 该函数用于将当前检测到的椎桶进行可视化输出，方便调试
*/
void Mapping::sendVisualCurrentConeDetections(std::string frame_id, fsd_common_msgs::ConeDetections cones)
{
    visualization_msgs::Marker cone_marker;
    visualization_msgs::MarkerArray cone_maker_array;

    //Set the marker type
    cone_marker.type = visualization_msgs::Marker::CUBE;
    cone_marker.action = visualization_msgs::Marker::ADD;

    cone_marker.header.stamp = ros::Time::now();
    cone_marker.header.frame_id = frame_id;
														   
    cone_marker.ns = "slam";

    cone_marker.lifetime = ros::Duration();
    cone_marker.frame_locked = true;

    //Set the color
    cone_marker.color.r = 1.0f;
    cone_marker.color.g = 0.0f;
    cone_marker.color.b = 0.0f;
    cone_marker.color.a = 0.8f;

    cone_marker.scale.x = 0.2;
    cone_marker.scale.y = 0.2;
    cone_marker.scale.z = 0.3;

    // default val to fix warn of rviz
    cone_marker.pose.orientation.x = 0;
    cone_marker.pose.orientation.y = 0;
    cone_marker.pose.orientation.z = 0;
    cone_marker.pose.orientation.w = 1;

    for(auto i: cones.cone_detections)
    {
        if(i.poseConfidence.data > 0.8)
        {
            cone_marker.id++;
            cone_marker.pose.position.x = i.position.x;
            cone_marker.pose.position.y = i.position.y;

            if(i.color.data == "r")
            {
                cone_marker.color.r = 1.0f;
                cone_marker.color.g = 0.0f;
                cone_marker.color.b = 0.0f;
            }
            else if(i.color.data == "b")
            {
                cone_marker.color.r = 0.0f;
                cone_marker.color.g = 0.0f;
                cone_marker.color.b = 1.0f;
            }
            else
            {
                cone_marker.color.r = 0.0f;
                cone_marker.color.g = 1.0f;
                cone_marker.color.b = 0.0f;
            }

            cone_maker_array.markers.push_back(cone_marker);
        }
    }

    current_visual_cone_detections_pub_.publish(cone_maker_array);
}

/***
 * @brief 该函数用于将已经建好的椎桶地图以及行使过的gps轨迹保存到txt文件中
*/
void Mapping::saveMap()
{
    //保存椎桶地图
    std::ofstream cone_map(map_save_path_ + "cone_map.txt", std::ofstream::out | std::ofstream::trunc);

    if (!cone_map.is_open())
    {
        std::cout << "Failed to open cone map!" << std::endl;
        return;
    }

    for(auto i: map_buffer_)
    {
        if(i.getUpdateCount() >= min_update_count_)
        {
            double temp_x = i.getCone().position.x;
            double temp_y = i.getCone().position.y;
            std::string temp_color = i.getCone().color.data;

            cone_map << temp_x << " " << temp_y << " " << temp_color << std::endl;
        }
    }

    cone_map.close();

    //保存gps轨迹地图
    std::ofstream gps_map(map_save_path_ + "gps_map.txt", std::ofstream::out | std::ofstream::trunc);

    if (!gps_map.is_open())
    {
        std::cout << "Failed to open gps map!" << std::endl;
        return;
    }

    for(auto j: track_line_.poses)
    {
        double temp_x = j.pose.position.x;
        double temp_y = j.pose.position.y;
        double temp_theta = j.pose.orientation.z;

        gps_map << temp_x << " " << temp_y << " " << temp_theta << std::endl;
    }

    gps_map.close();
}

/***
 * @brief 该函数将保存的地图信息加载，以便于后续跑动能够直接进行全局跑动
*/
void Mapping::loadMap()
{
    //检验地图文件是否为空
    std::ifstream check_cone_map(map_save_path_ + "cone_map.txt", std::ios::binary | std::ios::ate); // 打开文件并将指针移到末尾
    if (!check_cone_map.is_open()) 
    {
        std::cerr << "Could not open the file: " << map_save_path_ + "cone_map.txt" << std::endl;
        return;
    }
    std::streampos check_cone_map_size = check_cone_map.tellg(); // 获取文件大小
    check_cone_map.close();
    if(check_cone_map_size == 0)
    {
        ROS_WARN("椎桶地图文件为空文件");
        return;
    }

    std::ifstream check_gps_map(map_save_path_ + "gps_map.txt", std::ios::binary | std::ios::ate);
    if (!check_gps_map.is_open()) 
    {
        std::cerr << "Could not open the file: " << map_save_path_ + "gps_map.txt" << std::endl;
        return;
    }
    std::streampos check_gps_map_size = check_gps_map.tellg(); // 获取文件大小
    check_gps_map.close();
    if(check_gps_map_size == 0)
    {
        ROS_WARN("gps地图文件为空文件");
        return;
    }

    //加载椎桶地图文件
    std::ifstream cone_map(map_save_path_ + "cone_map.txt");

    if (!cone_map.is_open()) 
    {
        std::cerr << "cone map to open file!" << std::endl;
        return;
    }
    std::string cone_map_line;
    while(std::getline(cone_map, cone_map_line))
    {
        double temp_x, temp_y; std::string temp_color;
        std::istringstream cone_iss(cone_map_line);

        if (cone_iss >> temp_x >> temp_y >> temp_color) 
        {
            fsd_common_msgs::Cone temp_cone;
            temp_cone.color.data = temp_color;
            temp_cone.position.x = temp_x;
            temp_cone.position.y = temp_y;
            ConeInSlam temp_cone_in_slam(temp_cone);
            temp_cone_in_slam.setUpdateCount(999);
            map_buffer_.push_back(temp_cone_in_slam);

            std::cout << "Read values: " << temp_x << ", " << temp_y << ", " << temp_color << std::endl;
        } 
        else 
        {
            std::cerr << "Error parsing line!" << std::endl;
        }
    }

    //加载gps地图文件
    std::ifstream gps_map(map_save_path_ + "gps_map.txt");

    if (!gps_map.is_open()) 
    {
        std::cerr << "gps map to open file!" << std::endl;
        return;
    }
    std::string gps_map_line;
    while(std::getline(gps_map, gps_map_line))
    {
        double temp_x, temp_y, temp_theta;
        std::istringstream gps_iss(gps_map_line);

        if (gps_iss >> temp_x >> temp_y >> temp_theta)
        {
            geometry_msgs::PoseStamped temp_state;
            temp_state.pose.position.x = temp_x;
            temp_state.pose.position.y = temp_y;
            temp_state.pose.orientation.z = temp_theta;
            track_line_.poses.push_back(temp_state);

            std::cout << "Read values: " << temp_x << ", " << temp_y << ", " << temp_theta << std::endl;
        }
        else 
        {
            std::cerr << "Error parsing line!" << std::endl;
        }
    }
}

/***
 * @brief 该函数将slam中使用的CarTrack类型数据转换为规划使用的Path类型数据
*/
nav_msgs::Path Mapping::carstateTrack2Path()
{
    nav_msgs::Path temp_path;
    for(auto i: track_.track)
    {
        geometry_msgs::PoseStamped p;
        p.pose.position.x = i.car_state.x;
        p.pose.position.y = i.car_state.y;
        temp_path.poses.push_back(p);
    }

    return temp_path;
}

/***
 * @brief 该函数用于得到车辆两位置之间的距离
*/
double Mapping::getDeltaDis()
{
    double delta_x = current_state_.car_state.x - last_state_.car_state.x;
    double delta_y = current_state_.car_state.y - last_state_.car_state.y;

    double res = sqrt(delta_x*delta_x + delta_y*delta_y);

    return res;
}

/***
 * @brief 该函数用于将buffer中符合条件的椎桶提取出来生成地图
*/
fsd_common_msgs::ConeDetections Mapping::getMap()
{
    fsd_common_msgs::ConeDetections map;
    for(auto i: map_buffer_)
    {
        // std::cout << "#################################################################" << std::endl;
        // ROS_INFO("this cone's update times is %f", i.getUpdateCount());
        if(i.getUpdateCount() >= min_update_count_)
        {
            map.cone_detections.push_back(i.getCone());
        }
    }
    //ROS_INFO("get map %f ", map.cone_detections.size());
    return map;
}

/***
 * @brief 该函数用于将buffer中符合条件的椎桶提取出来生成地图，有颜色信息
*/
fsd_common_msgs::Map Mapping::getMapWithColor()
{
    fsd_common_msgs::Map map;
    for(auto i = map_buffer_.begin(); i != map_buffer_.end(); i++)
    {
        if(i->getUpdateCount() >= min_update_count_)
        {
            fsd_common_msgs::Cone temp = i->getCone();
            if(temp.color.data == "r") map.cone_red.push_back(temp);
            if(temp.color.data == "b") map.cone_blue.push_back(temp);
        }
    }
    
    return map;
}

/***
 * @brief 该函数用于开始进行slam
*/
void Mapping::startSlam()
{
    //std::cout << "#################################################################" << std::endl;
    
    regCones();

    fsd_common_msgs::ConeDetections realtime_map;
    realtime_map = Mapping::getMap();
    //ROS_INFO("realtime map size = %d", realtime_map.cone_detections.size());
    realtime_map_pub_.publish(realtime_map);

    Mapping::sendVisualization(frame_id_);

}