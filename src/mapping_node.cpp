#include "mapping.h"

using namespace racingslam;

int main(int argc,char** argv)
{
    ros::init(argc,argv,"mapping");

    Mapping handle = Mapping("");

    handle.loadParam();
    handle.regTopic();

    ros::spin();

    return 0;

}