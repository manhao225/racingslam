#include "ndt_odom.h"

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <Eigen/Eigen>

using namespace racingslam;

NDTOdometry::NDTOdometry(std::string ns) : Odometry(ns) {

    // last_cloud_ = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
    // current_cloud_ = boost::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
    // last_cloud_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
    // current_cloud_ = std::make_shared<pcl::PointCloud<pcl::PointXYZ>>();
    last_cloud_ = pcl::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>(new pcl::PointCloud<pcl::PointXYZ>());
    current_cloud_ = pcl::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>(new pcl::PointCloud<pcl::PointXYZ>());
    ndt_ = pcl::shared_ptr<pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>>(new pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>());
    // ndt_ = std::make_shared<pcl::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>>();

}

void NDTOdometry::loadParam() {

    loadBasicParam();
    
    if (!node_.param<std::string>("odometry/ndt/frame_name", odom_frame_name_, "odom_ndt")) {
        ROS_WARN_STREAM("Did not load odometry/ndt/frame_name. Standard value is: " << odom_frame_name_);
    }
    if (!node_.param<std::string>("odometry/ndt/topic", input_topic_name_, "/rslidar_points")) {
        ROS_WARN_STREAM("Did not load odometry/ndt/topic. Standard value is: " << input_topic_name_);
    }
    if (!node_.param<double>("odometry/ndt/min_diff", min_diff_, 0.01)) {
        ROS_WARN_STREAM("Did not load odometry/ndt/min_diff. Standard value is: " << min_diff_);
    }
    if (!node_.param<double>("odometry/ndt/step", step_, 0.1)) {
        ROS_WARN_STREAM("Did not load odometry/ndt/step. Standard value is: " << step_);
    }
    if (!node_.param<double>("odometry/ndt/resolutoin", resolution_, 1.0)) {
        ROS_WARN_STREAM("Did not load odometry/ndt/resolutoin. Standard value is: " << resolution_);
    }
    if (!node_.param<double>("odometry/ndt/max_iter", max_iter_, 100)) {
        ROS_WARN_STREAM("Did not load odometry/ndt/max_iter. Standard value is: " << max_iter_);
    }

}



void NDTOdometry::regTopics() {

    regBasicTopics();
    input_sub_ = node_.subscribe<sensor_msgs::PointCloud2>(input_topic_name_, 1, &NDTOdometry::inputCallback, this);
}



void NDTOdometry::inputCallback(const sensor_msgs::PointCloud2::ConstPtr &msg) {

    if(!is_inited_) {
        ROS_ERROR("Error: Odometry has not been inited!!!");
        return;
    }

    pcl::fromROSMsg(*msg, *current_cloud_);

    std::vector<int> indices; //储存处理过后有效点云的索引
    pcl::removeNaNFromPointCloud(*current_cloud_, *current_cloud_, indices);

    // convert cloud from lidar to car
    lidar2car(current_cloud_);

    // init when get first frame
    if(odom_track_.track.empty()) {

        last_cloud_ = current_cloud_->makeShared();
        current_cloud_.reset(new pcl::PointCloud<pcl::PointXYZ>);

        odom_track_.track.push_back(zero_pos_);

        //init ndt
        initNDT();

        // send tf
        sendTransform();

        return;

    }

    // Filter current cloud by down sampling, to increase speed of registration. 
    pcl::PointCloud<pcl::PointXYZ>::Ptr filted_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::ApproximateVoxelGrid<pcl::PointXYZ> filter;
    filter.setLeafSize (0.2, 0.2, 0.2); // filer cloud to 10% of original size. 
    filter.setInputCloud(current_cloud_);
    filter.filter(*filted_cloud);

    // Target --> Source
    ndt_->setInputSource(filted_cloud);
    ndt_->setInputTarget(last_cloud_);

    // Solve
    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    ndt_->align(*output_cloud);
    ROS_INFO("NDT converge status: %s; score: %f", ndt_->hasConverged() ? "True" : "False", ndt_->getFitnessScore());

    // get transform
    Eigen::Matrix4f transform = ndt_->getFinalTransformation();

    //update state
    fsd_common_msgs::CarState last_state = odom_track_.track.back();
    fsd_common_msgs::CarState current_state = transformCarState(last_state, transform.cast<double>());

    //add state in odom_track
    odom_track_.track.push_back(current_state);
    // send tf
    sendTransform();

    // update last pointcloud
    last_cloud_ = current_cloud_->makeShared();
    current_cloud_.reset(new pcl::PointCloud<pcl::PointXYZ>);

    odom_track_pub_.publish(odom_track_);
    sendCarTrackVisualization();

}



void NDTOdometry::initNDT() {

    if(!is_inited_) {
        ROS_ERROR("Error: Odometry has not been inited!!!");
        return;
    }

    ndt_->setTransformationEpsilon(min_diff_);
    ndt_->setStepSize(step_);
    ndt_->setResolution(resolution_);
    ndt_->setMaximumIterations(max_iter_);

}
